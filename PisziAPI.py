#!/usr/bin/python3

from flask import Flask, request
from flask_cors import CORS
import Corrector
import TopicsClallsifier
import SimilarPostsFinder
import json


app = Flask("__name__")
CORS(app)


@app.route("/api/v1/piszi", methods=["POST"])
def piszi():
    data = json.loads(request.get_data().decode('UTF-8'))
    answer = Corrector.correct(data)
    return json.dumps(answer, ensure_ascii=False).encode('UTF-8')


@app.route("/api/v1/topics", methods=["POST"])
def topics():
    text = request.get_data().decode('UTF-8')
    topics = TopicsClallsifier.get_topics(text)
    translated_topics = TopicsClallsifier.translate_topics(topics)
    retval = [v[0] for v in translated_topics]
    return json.dumps(retval, ensure_ascii=False).encode('UTF-8')


@app.route("/api/v1/related", methods=["POST"])
def related():
    text = request.get_data().decode('UTF-8')
    topics = TopicsClallsifier.get_topics(text)
    translated_topics = TopicsClallsifier.translate_topics(topics)
    topics = [v[0] for v in translated_topics]
    posts = SimilarPostsFinder.get_posts_by_tags(topics[:min(3, len(topics))])
    if len(posts) < 4:
        posts = SimilarPostsFinder.get_posts_by_tags(topics[:min(1, len(topics))])
    retval = [{"title": v[0], "url": v[1]} for v in posts]
    return json.dumps(retval, ensure_ascii=False).encode('UTF-8')
