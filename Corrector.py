#!/usr/bin/python3

import abc
import json
from json.decoder import JSONDecodeError

import pymystem3

import LanguageTool
import YandexSpeller


class JsonTokenFields:
    ORIGINAL = "original"
    VARIANTS = "variants"
    ERROR = "error"
    PRIVATE = "private"


class Corrector:
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def correct(self, data):
        pass


class YandexSpellerCorrector(Corrector):
    def correct(self, data):
        result = []
        for datum in data:
            if datum.get(JsonTokenFields.ERROR, "") or datum.get(JsonTokenFields.VARIANTS, []):
                continue
            private = datum.get(JsonTokenFields.PRIVATE, {})
            text = datum[JsonTokenFields.ORIGINAL]
            request = YandexSpeller.Request(text)
            response = request.perform()
            response.sort(key=lambda report: report["pos"])
            next_start = 0
            for report in response:
                if report["pos"] > next_start:
                    result.append({
                        JsonTokenFields.ORIGINAL: text[next_start:report["pos"]],
                        JsonTokenFields.PRIVATE: private
                    })
                    next_start = report["pos"]
                report["s"] = [alt for alt in report["s"] if alt != report["word"]]
                if len(report["s"]) == 0:
                    result.append({
                        JsonTokenFields.ORIGINAL: report["word"],
                        JsonTokenFields.PRIVATE: private
                    })
                else:
                    result.append({
                        JsonTokenFields.ERROR: "",
                        JsonTokenFields.ORIGINAL: report["word"],
                        JsonTokenFields.VARIANTS: report["s"],
                        JsonTokenFields.PRIVATE: private
                    })
                next_start += report["len"]
            if next_start < len(text):
                result.append({
                    JsonTokenFields.ORIGINAL: text[next_start:],
                    JsonTokenFields.PRIVATE: private
                })
        return result


class LexisCorrector(Corrector):
    def correct(self, data):
        original_text = get_original_text(data)
        mystem = pymystem3.Mystem()
        tokens = mystem.analyze(original_text)

        token_offset = 0
        for token in tokens:
            token_length = len(token["text"])

            if token_error_message(token) and not suggestion_is_shadowing(data, token_offset, token_length):

                new_data = []
                data_offset = 0
                for d in data:
                    text = d[JsonTokenFields.ORIGINAL]
                    data_length = len(d[JsonTokenFields.ORIGINAL])

                    if data_offset <= token_offset and token_offset + token_length <= data_offset + data_length:
                        prefix_length = token_offset - data_offset
                        suffix_length = data_length + data_offset - (token_length + token_offset)
                        if prefix_length:
                            new_data.append({
                                JsonTokenFields.ORIGINAL: text[:prefix_length],
                                JsonTokenFields.PRIVATE: d[JsonTokenFields.PRIVATE]
                            })
                        new_data.append({
                            JsonTokenFields.ORIGINAL: text[prefix_length:get_right_border(text, suffix_length)],
                            JsonTokenFields.ERROR: token_error_message(token),
                            JsonTokenFields.VARIANTS: [""],
                            JsonTokenFields.PRIVATE: d[JsonTokenFields.PRIVATE]
                        })
                        if suffix_length:
                            new_data.append({
                                JsonTokenFields.ORIGINAL: text[-suffix_length:],
                                JsonTokenFields.PRIVATE: d[JsonTokenFields.PRIVATE]})
                    else:
                        new_data.append(d)
                    data_offset += data_length
                data = new_data
            token_offset += token_length
        return data


class SyntaxAndPunctuationCorrector(Corrector):
    def correct(self, data):
        original_text = get_original_text(data)

        try:
            response = LanguageTool.Request(original_text).perform()
        except JSONDecodeError:
            return data

        for suggestion in response:
            suggestion_offset = suggestion["offset"]
            suggestion_length = suggestion["length"]

            if suggestion_is_shadowing(data, suggestion_offset, suggestion_length):
                continue

            data_offset = 0
            new_data = []
            for d in data:
                data_length = len(d[JsonTokenFields.ORIGINAL])
                if data_offset <= suggestion_offset and \
                        suggestion_offset + suggestion_length - 1 <= data_offset + data_length - 1:
                    text = d[JsonTokenFields.ORIGINAL]
                    prefix_length = suggestion_offset - data_offset
                    suffix_length = data_length + data_offset - (suggestion_length + suggestion_offset)
                    if prefix_length:
                        new_data.append({
                            JsonTokenFields.ORIGINAL: text[:prefix_length],
                            JsonTokenFields.PRIVATE: d[JsonTokenFields.PRIVATE]
                        })
                    new_data.append({
                        JsonTokenFields.ORIGINAL: text[prefix_length:get_right_border(text, suffix_length)],
                        JsonTokenFields.ERROR: suggestion["message"],
                        JsonTokenFields.VARIANTS: list(
                            [replacement["value"] for replacement in suggestion["replacements"]]),
                        JsonTokenFields.PRIVATE: d[JsonTokenFields.PRIVATE]
                    })
                    if suffix_length:
                        new_data.append({
                            JsonTokenFields.ORIGINAL: text[-suffix_length:],
                            JsonTokenFields.PRIVATE: d[JsonTokenFields.PRIVATE]})
                else:
                    new_data.append(d)
                data_offset += data_length
            data = new_data
        return data


def correct(object):
    yandex_speller = YandexSpellerCorrector()
    language_tool = SyntaxAndPunctuationCorrector()
    lexis_correcter = LexisCorrector()

    correcters = [yandex_speller, language_tool, lexis_correcter]

    for correcter in correcters:
        object = correcter.correct(object)

    return object


"""
    Checks if [a, b] and [c, d] overlap in at least one point.
"""


def segments_overlap(a, b, c, d):
    return a <= c <= b or c <= a <= d


def get_original_text(data):
    original_text = ""
    for d in data:
        original_text += d["original"]
    return original_text


def suggestion_is_shadowing(data, suggestion_offset, suggestion_length):
    data_offset = 0
    for d in data:
        data_length = len(d[JsonTokenFields.ORIGINAL])
        if d.get(JsonTokenFields.ERROR, "") or d.get(JsonTokenFields.VARIANTS, []):
            if segments_overlap(data_offset, data_offset + data_length - 1,
                                suggestion_offset, suggestion_offset + suggestion_length - 1):
                return True
        data_offset += data_length


class Lazy:
    def __init__(self, generator):
        self.generator = generator
        self.run = False

    def __call__(self):
        if not self.run:
            self.ret = self.generator()
            self.run = True
        return self.ret


@Lazy
def stopslov_net_db():
    with open('stopslov.net.json', 'r') as file:
        data = json.load(file)
    return set([s.lower() for s in data["words"]])


@Lazy
def bad_words_db():
    with open('bad_dictionary.json', 'r') as file:
        data = json.load(file)
    return data


def get_right_border(text, suff):
    if suff == 0:
        return len(text)
    else:
        return -suff


def token_error_message(token):
    if not token.get("analysis", []):
        return ""
    analysis = token["analysis"][0]
    if not analysis.get("gr", []) and analysis.get("qual", ""):
        return ""
    if analysis.get("gr", ""):
        gr = analysis["gr"]
        if gr.find("обсц") != -1:
            return "Обсценная лексика"
        if gr.find("разг") != -1:
            return "Разговорная лексика"
        if gr.find("устар") != -1:
            return "Устаревшая лексика"
    stopwords = stopslov_net_db()
    bad_words = bad_words_db()

    if analysis["lex"] in stopwords:
        return "Стоп-слово"
    if analysis["lex"] in bad_words:
        return bad_words[analysis["lex"]] + " лексика"
    return ""
