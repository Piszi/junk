import unittest

from Corrector import YandexSpellerCorrector


class TestSpeller(unittest.TestCase):
    @staticmethod
    def create_simple_json(s: str):
        return [{"original": s, "private": ""}]

    @staticmethod
    def create_error_json(text: str, variants: list, err: str = ""):
        return [{"error": err, "original": text, "variants": variants, "private": ""}]

    @classmethod
    def setUp(cls):
        cls.correcter = YandexSpellerCorrector()

    def test_empty_input(self):
        self.assertEqual(self.correcter.correct([]), [])

    def test_empty_string_input(self):
        json = self.correcter.correct(self.create_simple_json(""))
        self.assertEqual(json, json)

    def test_correct_word(self):
        json = self.correcter.correct(self.create_simple_json("слово"))
        self.assertEqual(json, json)

    def test_correct_sentence(self):
        json = self.correcter.correct(self.create_simple_json("В начале было слово"))
        self.assertEqual(json, json)

    def test_invalid_word_missing_letter(self):
        text = "Малчик"
        self.assertEqual(self.correcter.correct(
            self.create_simple_json(text)),
            self.create_error_json(text, ["Мальчик", "Мальчику", "Мальчика"]))

    def test_invalid_word_odd_letter(self):
        text = "Булочька"
        self.assertEqual(self.correcter.correct(
            self.create_simple_json(text)),
            self.create_error_json(text, ["Булочка", "Белочка", "Булочки"]))

    def test_invalid_simple_collocation(self):
        text = "Я сделаль"
        self.assertEqual(self.correcter.correct(
            self.create_simple_json(text)),
            self.create_simple_json("Я ") + self.create_error_json("сделаль",
                                                                   ["сделал", "сделать", "сделала", "сделали"]))

    def test_invalid_resolving_context_collocation(self):
        text = "Совсем невиновен"
        self.assertEqual(self.correcter.correct(
            self.create_simple_json(text)),
            self.create_simple_json("Совсем ") + self.create_error_json("невиновен", ["не виновен"]))

    def test_invalid_context_collocation(self):
        text = "скучать музыку"
        self.assertEqual(self.correcter.correct(
            self.create_simple_json(text)), self.create_error_json("скучать", ["скачать", "скочать", "скчать"])
                                            + self.create_simple_json(" музыку"))
