import requests
import textrazor

TEXTRAZOR_API_KEY = "9f1a1e47adc0e0411cafb01c11dec3b2e42780c4a1b74235b8da2b8d"
YANDEX_TRANSLATE_API_KEY = "trnsl.1.1.20181110T124453Z.b29f30a97e343661.ae712f35d485fb3afc0befafd3bd58cfbd02e86b"

YANDEX_TRASLATE_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate"


def get_topics(text: str):
    textrazor.api_key = TEXTRAZOR_API_KEY
    client = textrazor.TextRazor(extractors=['topics'])
    response = client.analyze(text)
    return filter(lambda topic:  topic[1] >= 0.9, [(topic.label, topic.score) for topic in response.topics()[:15]])


def translate_topics(topics: list):
    translated_topics = []
    for topic, score in topics:
        headers = {"content-type": "application/x-www-form-urlencoded", "accept": "application/json"}
        response = requests.post(
            YANDEX_TRASLATE_URL,
            data="key={}&text={}&lang=en-ru".format(YANDEX_TRANSLATE_API_KEY, topic),
            headers=headers)
        translated_topics.append((response.json()["text"][0], score))
    return translated_topics
