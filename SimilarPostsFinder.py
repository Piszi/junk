import re

from googleapiclient.discovery import build

my_api_key = "AIzaSyBm2c9BLHlsNsa0w2fwIdXEcy1r5iCIvBE"
my_cse_id = "000926023877369861469:mf8-aybnr4q"


def google_search(search_term, api_key, cse_id, **kwargs):
    service = build("customsearch", "v1", developerKey=api_key)
    res = service.cse().list(q=search_term, cx=cse_id, **kwargs).execute()
    if 'items' not in res:
        return []
    return res['items']


def get_posts_by_tags(tags):
    results = google_search("site:postnauka.ru " + str.join(" ", tags), my_api_key, my_cse_id, num=10)
    matching_articles = filter(lambda result: re.match(r"https://postnauka\.ru/[a-z]+/[0-9]+", result["link"]), results)
    return list(map(lambda result: (result["title"], result["link"]), matching_articles))[:6]