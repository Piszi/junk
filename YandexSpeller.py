#!/usr/bin/python3

import requests


CHECK_TEXT_API_ADDR = "https://speller.yandex.net/services/spellservice.json/checkText"


class Options:
    IGNORE_DIGITS = 2
    IGNORE_URLS = 4
    FIND_REPEAT_WORDS = 8
    IGNORE_CAPITALIZATION = 512


class Request:
    def __init__(self, text, lang="ru", options=Options.FIND_REPEAT_WORDS, format="plain"):
        self.text = text
        self.lang = lang
        self.options = options
        self.format = format

    def perform(self):
        params = {
            "text": self.text,
            "lang": self.lang,
            "options": self.options,
            "format": self.format
        }
        r = requests.get(CHECK_TEXT_API_ADDR, params=params)
        return r.json()


def format_response(data):
    print(data)
    for error in data:
        code = error["code"]
        if code == 1:
            desc = "ERROR_UNKNOWN_WORD"
        elif code == 2:
            desc = "ERROR_REPEAT_WORD"
        elif code == 3:
            desc = "ERROR_CAPITALIZATION"
        else:
            desc = "ERROR_TOO_MANY_ERRORS"
        print(desc, error["word"], error["s"])


if __name__ == '__main__':
    t = input()
    response = Request(t, options=0).perform()
    format_response(response)
