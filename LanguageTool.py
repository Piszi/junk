import requests

LANGUAGE_TOOL_URL = "https://languagetool.org/api/v2/check"


class Request:
    __headers = {"content-type": "application/x-www-form-urlencoded", "accept": "application/json"}

    def __init__(self, text, lang="ru-RU", restrictChecksSet=False):
        self.text = text.encode('utf-8').decode('latin1')
        self.lang = lang
        self.enabledOnly = restrictChecksSet

    def perform(self):
        response = requests.post(
            LANGUAGE_TOOL_URL,
            data="text={}&language={}&enabledOnly={}".format(self.text, self.lang, self.enabledOnly),
            headers=self.__headers)
        return response.json()["matches"]
